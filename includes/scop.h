/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scop.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hbouillo <hbouillo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/28 10:24:50 by vtudes            #+#    #+#             */
/*   Updated: 2020/11/24 03:01:58 by hbouillo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCOP_H
# define SCOP_H

# include "libft.h"
# include "libmatrices.h"
# include <math.h>
# include <stdio.h>
# include <errno.h>
# include <stdlib.h>
# include <unistd.h>
# include <GL/glew.h>
# include <GLFW/glfw3.h>

/*
** Modificated ligne 234 lib>glfw>include>glfw3.h
** from #include <OpenGL/gl.h> to
** #include <OpenGL/gl3.h>
*/
# define OPENGL_VERSION "4.0"
# define GLFW_VERSION 4.0

# define WINDOW_W 1280
# define WINDOW_H 720
# define CAMERA_NEAR 0.001f
# define CAMERA_FAR 100.0f
# define CAMERA_FOV 60.0f
# define BUFFER_SIZE 128
# define COOLDOWN 20

# define VERT_SHADER "./shader/vertex_obj_view.glsl"
# define FRAG_SHADER "./shader/fragment_obj_view.glsl"

# define ROTATION_SPEED 1.5f
# define ZOOM_MAX 40
# define ZOOM_MIN 0
# define ZOOM_SPEED 0.5f

# define PITCH_UP 1
# define PITCH_DOWN 2

# define MAX_KEYS 348
# define ESC GLFW_KEY_ESCAPE
# define KEY_D GLFW_KEY_D
# define KEY_A GLFW_KEY_A
# define KEY_W GLFW_KEY_W
# define KEY_S GLFW_KEY_S
# define KEY_Q GLFW_KEY_Q
# define KEY_E GLFW_KEY_E
# define SPACE GLFW_KEY_SPACE
# define KEY_J GLFW_KEY_J
# define KEY_K GLFW_KEY_K
# define KEY_T GLFW_KEY_T
# define KEY_C GLFW_KEY_C
# define KEY_1 GLFW_KEY_1
# define KEY_2 GLFW_KEY_2
# define KEY_3 GLFW_KEY_3
# define KEY_H GLFW_KEY_H

# define KEY_RIGHT GLFW_KEY_RIGHT
# define KEY_LEFT GLFW_KEY_LEFT
# define KEY_UP GLFW_KEY_UP
# define KEY_DOWN GLFW_KEY_DOWN
# define KEY_HOME GLFW_KEY_HOME
# define KEY_END GLFW_KEY_END

typedef struct	s_movipr
{
	t_mat4	model;
	t_mat4	view;
	t_mat4	projection;
	t_mat4	mvp;
}				t_movipr;

/*
** bpp : bit par pixel
** opp : opacity per pixel
*/
typedef struct	s_text
{
	unsigned char	*image;
	int				size;
	int				wi;
	int				he;
	int				sl;
	short			bpp;
	short			opp;
}				t_text;

typedef struct	s_win
{
	GLFWwindow	*ptr;
	int			width;
	int			height;
	float		ratio;
}				t_win;

/*
** Base yaw 45.0f and base pitch 15.0f
*/
typedef struct	s_rot
{
	float		yaw;
	float		pitch;
	float		roll;
	float		distance;
}				t_rot;

/*
** pos : camera position in world space
** targetc : where i want to loot at, in the world space
*/
typedef struct	s_camera
{
	t_vec3		pos;
	t_vec3		target;
	t_vec3		up_vector;
	t_rot		rota;
	float		auto_rot;
}				t_camera;

typedef struct	s_key
{
	short	code;
	short	cooldown;
}				t_key;

typedef struct	s_shaders
{
	GLuint		program;
	GLint		mvp_loc;
	GLint		cmd_loc;
	GLint		smd_loc;
	GLint		tmd_loc;
	GLint		gmd_loc;
	GLint		mmd_loc;
	GLint		tex_loc;

}				t_shaders;

typedef struct	s_uniforms
{
	GLint		mvp_loc;
	GLint		cmd_loc;
	GLint		smd_loc;
	GLint		tmd_loc;
	GLint		gmd_loc;
	GLint		mmd_loc;
	GLint		tex_loc;
	GLint		opa_loc;
}				t_uniforms;

typedef struct	s_settings
{
	float		fov;
	float		aspect_ratio;
	float		near;
	float		far;
}				t_settings;

typedef struct	s_obj
{
	char			*filename;
	GLfloat			*vertex;
	GLuint			*indices;
	unsigned int	vertex_size;
	unsigned int	indices_size;
	unsigned int	indices_num;
	t_mat4			translation;
	t_mat4			rotation;
	t_vec3			axis_center;
	t_vec3			inertia;
	float			velocity;
	t_text			texture;
}				t_obj;

/*
** wireframe : render primitive as wireframe
*/
typedef struct	s_model
{
	short	wireframe;
	short	focus;
	short	shading;
	short	color;
	short	greyscale;
	short	mapping;
	short	texture;
	float	opacity;
	short	target_opacity;
	t_vec3	pos;
}				t_model;

typedef struct	s_buffer
{
	GLuint	vao;
	GLuint	vbo;
	GLuint	ebo;
	GLuint	texture;
}				t_buffer;

GLuint			create_program(char *vertex_shader_path, \
								char *fragment_shader_path);

t_obj			parse_object(int fd);
void			view_object(t_obj *obj, t_camera *camera, \
							t_settings *settings, GLFWwindow *window);

t_settings		init_settings(void);
t_camera		init_camera(void);
t_rot			init_rot(void);
t_movipr		init_mvp(t_settings *settings);
void			compute_mvp(t_movipr *movipr);
void			look_at(t_movipr *movipr, t_camera *camera);
t_vec3			get_position_fixed_dist(t_vec3 target, float dist, \
										float yaw, float pitch);
void			malloc_obj(t_obj *obj);

t_buffer		allocate_buffers();
void			bind_buffers(t_buffer *buf);
void			fill_buffers(t_obj *obj, int mode);
void			create_texture(t_buffer *buf, t_obj *obj);

void			key_events(GLFWwindow *window, t_camera *cam, t_model *mod);
void			key_toggle(t_key *key, short *var, int v0, int v1);

void			parse_bmp(t_obj *obj, char *filename);

/*
** loop handling
*/
t_camera		*auto_rot(t_camera *cam);
void			init_loop(t_obj *obj, t_buffer *buf);
void			end_loop(GLFWwindow *window);
void			draw_model(t_buffer *buf, t_obj *obj);
void			handle_textures(t_model *model, t_buffer *buf);
void			toggle_wireframe(t_model *model);
void			compute_and_draw(t_movipr *movipr, t_buffer *buf, t_obj *obj);

void			toggle_texture(t_model *mod);

/*
** Utilitaries
*/
GLuint			*gluint_array_copy(GLuint *array, int length, int m);
void			error(char *info);
int				array_len(void **tab);
void			display_help();

/*
** Frre obj
*/
void			free_all(t_obj *obj, t_buffer *buf);

/*
** Ressources :
** http://www.opengl-tutorial.org/beginners-tutorials/tutorial-3-matrices/
** https://www.glfw.org/docs/3.3/quick.html
** https://www.khronos.org/registry/OpenGL-Refpages/gl4
*/

#endif
