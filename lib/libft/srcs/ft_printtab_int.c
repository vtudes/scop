/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printtab_int.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vtudes <vtudes@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/08 21:00:06 by vtudes            #+#    #+#             */
/*   Updated: 2020/09/28 10:39:38 by vtudes           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_printtab_int(int **tab)
{
	int		j;
	int		k;

	j = 0;
	while (tab[j])
	{
		k = 0;
		while (tab[j][k])
		{
			ft_putnbr(tab[j][k]);
			k++;
		}
		if (tab[j])
			ft_putchar('\n');
		j++;
	}
}
