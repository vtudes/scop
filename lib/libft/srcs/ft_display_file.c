/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_file.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vtudes <vtudes@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/28 10:36:57 by vtudes            #+#    #+#             */
/*   Updated: 2020/09/28 10:36:58 by vtudes           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

void	ft_display_file(char *argv)
{
	int		fd;
	char	buff[25];

	fd = open(argv, O_RDONLY);
	while (read(fd, &buff, 1) != 0)
		write(1, buff, 1);
	close(fd);
}
