/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printlst_char.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vtudes <vtudes@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/13 22:38:45 by vtudes            #+#    #+#             */
/*   Updated: 2020/09/28 10:39:30 by vtudes           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_printlst_char(t_list *chain)
{
	t_list		*cp;

	cp = chain;
	while (cp)
	{
		ft_putendl(cp->content);
		cp = cp->next;
	}
}
