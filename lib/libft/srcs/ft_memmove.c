/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vtudes <vtudes@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/13 17:22:52 by vtudes            #+#    #+#             */
/*   Updated: 2020/09/28 10:39:16 by vtudes           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	char	*source;
	char	*dest;

	source = (char*)src;
	dest = (char*)dst;
	len--;
	if (src > dst)
		return (ft_memcpy(dst, src, len + 1));
	while ((int)len >= 0)
	{
		dest[len] = source[len];
		len--;
	}
	return (dst);
}
