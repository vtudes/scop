/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_tab.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vtudes <vtudes@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/28 10:36:20 by vtudes            #+#    #+#             */
/*   Updated: 2020/09/28 10:36:21 by vtudes           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		*ft_atoi_tab(char **str)
{
	size_t	i;
	size_t	j;
	int		*tab;

	i = 0;
	while (str[i])
		i++;
	if (!(tab = (int*)malloc(sizeof(int) * i)))
		return (NULL);
	i = 0;
	while (str[i])
	{
		j = 0;
		while (str[i][j])
		{
			tab[i] = ft_atoi(str[i]);
			j++;
		}
		i++;
	}
	return (tab);
}
