/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vtudes <vtudes@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 20:07:58 by vtudes            #+#    #+#             */
/*   Updated: 2020/09/28 10:42:44 by vtudes           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(char const *s, int c)
{
	int		i;

	i = ft_strlen(s);
	if (c == 0)
		return ((char *)&s[i]);
	while (i != 0)
	{
		i--;
		if (s[i] == c)
			return ((char *)&s[i]);
	}
	return (NULL);
}
