/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vtudes <vtudes@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/21 11:24:44 by vtudes            #+#    #+#             */
/*   Updated: 2020/09/28 10:38:36 by vtudes           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*dst;
	t_list	*tempo;

	if (!(dst = ft_lstnew(NULL, 0)))
		return (NULL);
	dst = f(lst);
	tempo = dst;
	while (lst->next)
	{
		tempo->next = f(lst->next);
		if (tempo->next == NULL)
			return (NULL);
		lst = lst->next;
		tempo = tempo->next;
	}
	return (dst);
}
