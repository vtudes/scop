/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vtudes <vtudes@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/08 15:39:48 by vtudes            #+#    #+#             */
/*   Updated: 2020/09/28 10:42:51 by vtudes           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(char const *src, char const *to_find)
{
	int i;

	if ((!*src) && (!*to_find))
		return ((char *)src);
	if (!*src)
		return (NULL);
	i = -1;
	while (to_find[++i])
		if (to_find[i] != src[i])
			return (ft_strstr(src + 1, to_find));
	return ((char *)src);
}
