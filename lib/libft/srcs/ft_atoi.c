/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vtudes <vtudes@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/28 10:36:34 by vtudes            #+#    #+#             */
/*   Updated: 2020/09/28 10:36:35 by vtudes           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi(char const *str)
{
	int		val;
	int		i;
	int		sign;

	i = 0;
	val = 0;
	while (ft_isdigit(str[i]) == 0)
		i++;
	sign = (str[i - 1] == '-') ? -1 : 1;
	while (str[i] && (str[i] >= 48 && str[i] <= 57))
		val = val * 10 + (str[i++] - '0');
	return (sign * val);
}
