/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vtudes <vtudes@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 15:46:27 by vtudes            #+#    #+#             */
/*   Updated: 2020/09/28 10:42:12 by vtudes           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncat(char *dest, char const *src, size_t nb)
{
	int			i;
	size_t		j;

	i = 0;
	j = 0;
	while (dest[i] != '\0')
		i++;
	if (dest[i] == '\0')
	{
		dest[i] = src[j];
		while (src[j] != '\0' && j < nb)
		{
			dest[i] = src[j];
			i++;
			j++;
		}
	}
	if (j == nb)
		dest[i] = '\0';
	if (src[j] == '\0')
		dest[i] = '\0';
	return (dest);
}
