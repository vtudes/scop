/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vtudes <vtudes@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/08 11:01:15 by vtudes            #+#    #+#             */
/*   Updated: 2020/09/28 10:40:18 by vtudes           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr_fd(int nb, int fd)
{
	if (nb == -2147483648)
		ft_putstr_fd("-2147483648", fd);
	if ((-2147483648 < nb) || (nb >= 2147483647))
	{
		if (nb < 0)
		{
			ft_putchar_fd('-', fd);
			nb = nb * -1;
		}
		if (nb > 9)
		{
			ft_putnbr_fd(nb / 10, fd);
			nb = nb % 10;
			ft_putchar_fd(nb + '0', fd);
		}
		else
			ft_putchar_fd(nb + '0', fd);
	}
}
