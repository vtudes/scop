/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlower.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vtudes <vtudes@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/28 10:42:01 by vtudes            #+#    #+#             */
/*   Updated: 2020/09/28 10:44:17 by vtudes           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strlower(char *src)
{
	int		i;
	char	*dest;

	i = -1;
	dest = ft_strdup(src);
	while (src[++i])
		dest[i] = (char)ft_tolower((int)src[i]);
	return (dest);
}
