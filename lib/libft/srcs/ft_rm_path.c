/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rm_path.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vtudes <vtudes@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/19 21:28:57 by vtudes            #+#    #+#             */
/*   Updated: 2020/09/28 10:40:43 by vtudes           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_rm_path(char *str)
{
	char	*result;
	int		i;
	int		j;

	i = 0;
	while (str[i])
	{
		if (str[i] == '/')
			j = i;
		i++;
	}
	j++;
	if (!(result = (char *)malloc(sizeof(char) * (ft_strlen(str) - j + 1))))
		exit(1);
	i = 0;
	while (str[j])
		result[i++] = str[j++];
	result[j] = '\0';
	return (result);
}
