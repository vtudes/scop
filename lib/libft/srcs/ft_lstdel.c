/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vtudes <vtudes@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/21 11:10:51 by vtudes            #+#    #+#             */
/*   Updated: 2020/09/28 10:38:16 by vtudes           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	ft_lstdel(t_list **alst, void (*del)(void *))
{
	t_list		*list;
	t_list		*tmp;

	if (alst && del && *alst)
	{
		list = *alst;
		tmp = list;
		while (list)
		{
			tmp = tmp->next;
			del(list->content);
			free(list);
			list = NULL;
			list = tmp;
		}
		*alst = NULL;
	}
}
