/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_base.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vtudes <vtudes@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/28 10:37:29 by vtudes            #+#    #+#             */
/*   Updated: 2020/09/28 10:37:30 by vtudes           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

int			ft_is_base(char *nbr, char *base)
{
	int		i;

	i = 0;
	while (nbr[i])
	{
		if (ft_strchr(base, ft_toupper(nbr[i])) == NULL)
			return (0);
		i++;
	}
	return (1);
}
