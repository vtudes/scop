/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vtudes <vtudes@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 23:32:52 by vtudes            #+#    #+#             */
/*   Updated: 2020/09/28 10:39:00 by vtudes           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(void const *s, int c, size_t n)
{
	size_t			i;
	unsigned char	*k;

	k = (unsigned char*)s;
	i = 0;
	while (i < n && k[i] != (unsigned char)c)
		i++;
	if (i == n)
		return (NULL);
	return ((void *)(s + i));
}
