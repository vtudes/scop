/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   buffers.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vtudes <vtudes@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/24 01:57:58 by vtudes            #+#    #+#             */
/*   Updated: 2020/09/24 01:57:59 by vtudes           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

void			create_texture(t_buffer *buf, t_obj *obj)
{
	glGenTextures(1, &buf->texture);
	glBindTexture(GL_TEXTURE_2D, buf->texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, obj->texture.wi,
	obj->texture.he, 0, GL_RGB, GL_UNSIGNED_BYTE, obj->texture.image);
	glGenerateMipmap(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);
}

t_buffer		allocate_buffers(void)
{
	t_buffer	buf;

	glGenBuffers(1, &buf.vbo);
	glGenBuffers(1, &buf.ebo);
	glGenVertexArrays(1, &buf.vao);
	return (buf);
}

void			bind_buffers(t_buffer *buf)
{
	glBindVertexArray(buf->vao);
	glBindBuffer(GL_ARRAY_BUFFER, buf->vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buf->ebo);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat),
		(GLvoid*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat),
		(GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);
}

void			fill_buffers(t_obj *obj, int mode)
{
	glBufferData(GL_ARRAY_BUFFER, obj->vertex_size,
		obj->vertex, mode);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, obj->indices_size,
		obj->indices, mode);
}
