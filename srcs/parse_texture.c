/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_texture.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vtudes <vtudes@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/25 06:55:07 by vtudes            #+#    #+#             */
/*   Updated: 2020/09/29 14:48:48 by vtudes           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

static void	read_header(char *filename, t_text *texture)
{
	FILE	*file;

	if ((file = fopen(filename, "r")) == NULL)
		error("bmp file opening (fopen) failed.");
	fseek(file, 18, SEEK_SET);
	fread(&texture->wi, 4, 1, file);
	fread(&texture->he, 4, 1, file);
	fseek(file, 2, SEEK_CUR);
	fread(&texture->bpp, 2, 1, file);
	fclose(file);
	texture->opp = texture->bpp / 8;
	texture->sl = texture->wi * texture->opp;
	texture->wi < 0 ? texture->wi = -texture->wi : 0;
	texture->he < 0 ? texture->he = -texture->he : 0;
	texture->size = texture->sl * texture->he;
}

static void	get_image(t_text *texture, char *buffer, int i)
{
	int	h;
	int	j;
	int	size;

	h = 0;
	size = texture->size * 2;
	texture->image = (unsigned char*)malloc(sizeof(unsigned char) * size);
	while (i >= 0)
	{
		i -= texture->sl;
		j = 0;
		while (j < texture->sl)
		{
			texture->image[h + j] = (unsigned char)buffer[i + j + 2];
			texture->image[h + j + 1] = (unsigned char)buffer[i + j + 1];
			texture->image[h + j + 2] = (unsigned char)buffer[i + j];
			j += 3;
		}
		h += texture->sl;
	}
}

void		parse_bmp(t_obj *obj, char *filename)
{
	int		fd;
	int		i;
	char	*buffer;

	read_header(filename, &obj->texture);
	buffer = (char*)malloc(sizeof(char) * obj->texture.size + 1);
	if ((fd = open(filename, O_RDWR)) == -1)
		error("bmp file opening failed.");
	lseek(fd, 54, SEEK_SET);
	i = read(fd, buffer, obj->texture.size);
	get_image(&obj->texture, buffer, i);
	ft_strdel((char**)&buffer);
	close(fd);
}
