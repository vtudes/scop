/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shaders.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vtudes <vtudes@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/25 07:00:01 by vtudes            #+#    #+#             */
/*   Updated: 2020/09/25 07:01:59 by vtudes           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

static const GLchar		*get_shader_source(char *filename)
{
	int		fd;
	int		ret;
	char	buffer[BUFFER_SIZE + 1];
	char	*source;
	char	*del;

	source = ft_strnew(BUFFER_SIZE);
	if ((fd = open(filename, O_RDONLY)) == -1)
	{
		ft_putstr("shader source file opening failed.\n");
		exit(1);
	}
	while ((ret = read(fd, buffer, BUFFER_SIZE)))
	{
		buffer[ret] = '\0';
		del = source;
		source = ft_strjoin(source, buffer);
		ft_strdel(&del);
	}
	close(fd);
	return (source);
}

static GLuint			create_shader(char *filename, int shader_type)
{
	GLint			success;
	GLuint			shader;
	const GLchar	*shader_source;

	shader_source = get_shader_source(filename);
	shader = glCreateShader(shader_type);
	glShaderSource(shader, 1, &shader_source, NULL);
	glCompileShader(shader);
	free((void*)shader_source);
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		ft_putstr("Error: Shader compilation failed.\n");
		exit(1);
	}
	return (shader);
}

static GLuint			build_shader_program(GLuint shader_vert, \
						GLuint shader_frag)
{
	GLint	success;
	GLuint	shader_program;

	shader_program = glCreateProgram();
	glAttachShader(shader_program, shader_vert);
	glAttachShader(shader_program, shader_frag);
	glLinkProgram(shader_program);
	glGetProgramiv(shader_program, GL_LINK_STATUS, &success);
	if (!success)
	{
		ft_putstr("Error: Shader program compilation failed.\n");
		exit(1);
	}
	glDetachShader(shader_program, shader_vert);
	glDetachShader(shader_program, shader_frag);
	glDeleteShader(shader_vert);
	glDeleteShader(shader_frag);
	return (shader_program);
}

GLuint					create_program(char *vertex_shader_path, \
						char *fragment_shader_path)
{
	GLuint	shader_vert;
	GLuint	shader_frag;

	shader_vert = create_shader(vertex_shader_path, GL_VERTEX_SHADER);
	shader_frag = create_shader(fragment_shader_path, GL_FRAGMENT_SHADER);
	return (build_shader_program(shader_vert, shader_frag));
}
