/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_all.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vtudes <vtudes@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/29 14:18:07 by vtudes            #+#    #+#             */
/*   Updated: 2020/09/29 15:08:34 by vtudes           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

void			free_all(t_obj *obj, t_buffer *buf)
{
	free(obj->vertex);
	free(obj->indices);
	free(obj->texture.image);
	glDeleteBuffers(1, &buf->ebo);
	glDeleteBuffers(1, &buf->vbo);
	glDeleteVertexArrays(1, &buf->vao);
}
