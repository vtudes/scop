/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hbouillo <hbouillo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/25 06:55:13 by vtudes            #+#    #+#             */
/*   Updated: 2020/11/24 02:58:30 by hbouillo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

void				toggle_texture(t_model *mod)
{
	if (mod->opacity < mod->target_opacity)
		mod->opacity += 0.01f;
	else if (mod->opacity > mod->target_opacity)
		mod->opacity -= 0.01f;
	if (mod->opacity >= 1)
		mod->opacity = 1;
	if (mod->opacity <= 0)
		mod->opacity = 0;
}

static t_uniforms	bind_uniforms(GLuint program)
{
	t_uniforms		uniforms;

	uniforms.mvp_loc = glGetUniformLocation(program, "mvp");
	uniforms.smd_loc = glGetUniformLocation(program, "smod");
	uniforms.cmd_loc = glGetUniformLocation(program, "cmod");
	uniforms.tmd_loc = glGetUniformLocation(program, "tmod");
	uniforms.gmd_loc = glGetUniformLocation(program, "gmod");
	uniforms.mmd_loc = glGetUniformLocation(program, "mmod");
	uniforms.tex_loc = glGetUniformLocation(program, "ltexture");
	uniforms.opa_loc = glGetUniformLocation(program, "opacity");
	return (uniforms);
}

static void			update_uniforms(t_uniforms *uniforms, t_model *model,
						t_movipr *movipr)
{
	glUniformMatrix4fv(uniforms->mvp_loc, 1, GL_FALSE, movipr->mvp.m);
	glUniform1i(uniforms->smd_loc, model->shading);
	glUniform1i(uniforms->cmd_loc, model->color);
	glUniform1i(uniforms->gmd_loc, model->greyscale);
	glUniform1i(uniforms->mmd_loc, model->mapping);
	glUniform1i(uniforms->tmd_loc, model->texture);
	glUniform1f(uniforms->opa_loc, model->opacity);
}

static t_model		init_model(void)
{
	t_model model;

	model.wireframe = 0;
	model.shading = 0;
	model.focus = 0;
	model.color = 0;
	model.greyscale = 0;
	model.mapping = 0;
	model.texture = 1;
	model.opacity = 0.0f;
	model.target_opacity = 0;
	set_vecteurs(&model.pos, 0.0f);
	return (model);
}

void				apply_roll(t_movipr *movipr, float roll)
{
	t_mat4			rotation;
	float			roll_rad;

	roll_rad = roll * M_PI / 180.0f;
	matrices_set(&rotation, IDENTITY);
	rotation.m[0] = cos(roll_rad);
	rotation.m[1] = -sin(roll_rad);
	rotation.m[4] = sin(roll_rad);
	rotation.m[5] = cos(roll_rad);
	movipr->view = matrices_multiplicate(rotation, movipr->view);
}

void				model_pos(t_movipr *movipr, t_model *mod)
{
	matrices_set(&movipr->model, IDENTITY);
	movipr->model.m[3] = mod->pos.v[0];
	movipr->model.m[7] = mod->pos.v[1];
	movipr->model.m[11] = mod->pos.v[2];
}

void				view_object(t_obj *obj, t_camera *camera, \
					t_settings *settings, GLFWwindow *window)
{
	GLuint			program;
	t_model			model;
	t_uniforms		uniforms;
	t_movipr		movipr;
	t_buffer		buf;

	program = create_program(VERT_SHADER, FRAG_SHADER);
	model = init_model();
	uniforms = bind_uniforms(program);
	movipr = init_mvp(settings);
	buf = allocate_buffers();
	init_loop(obj, &buf);
	while (glfwWindowShouldClose(window) == 0)
	{
		key_events(window, camera, &model);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		camera = auto_rot(camera);
		model_pos(&movipr, &model);
		look_at(&movipr, camera);
		apply_roll(&movipr, camera->rota.roll);
		glUseProgram(program);
		update_uniforms(&uniforms, &model, &movipr);
		handle_textures(&model, &buf);
		compute_and_draw(&movipr, &buf, obj);
		end_loop(window);
	}
	free_all(obj, &buf);
}
