/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   compute_and_draw.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vtudes <vtudes@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/25 07:08:37 by vtudes            #+#    #+#             */
/*   Updated: 2020/09/29 14:12:01 by vtudes           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

void				compute_and_draw(t_movipr *movipr, t_buffer *buf, \
					t_obj *obj)
{
	compute_mvp(movipr);
	draw_model(buf, obj);
}
