/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_main.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hbouillo <hbouillo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/24 01:58:06 by vtudes            #+#    #+#             */
/*   Updated: 2020/11/23 17:25:52 by hbouillo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

t_settings		init_settings(void)
{
	t_settings		settings;

	settings.fov = CAMERA_FOV;
	settings.aspect_ratio = ((float)WINDOW_W) / ((float)WINDOW_H);
	settings.near = CAMERA_NEAR;
	settings.far = CAMERA_FAR;
	display_help();
	return (settings);
}

t_rot			init_rot(void)
{
	t_rot			rot;

	rot.yaw = 45.0f;
	rot.pitch = 15.0f;
	rot.roll = 0.0f;
	rot.distance = 8.0f;
	return (rot);
}

t_camera		init_camera(void)
{
	t_camera		camera;

	camera.target = vec3(0.0f, 0.0f, 0.0f);
	camera.pos = get_position_fixed_dist(camera.target, 8.0, -45.0, 45.0);
	camera.up_vector = vec3(0.0f, 1.0f, 0.0f);
	camera.auto_rot = ROTATION_SPEED;
	return (camera);
}
