/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mvp.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hbouillo <hbouillo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/24 01:58:26 by vtudes            #+#    #+#             */
/*   Updated: 2020/11/24 02:52:11 by hbouillo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

t_movipr		init_mvp(t_settings *settings)
{
	t_movipr	movipr;
	float		s;

	matrices_set(&movipr.model, IDENTITY);
	matrices_set(&movipr.view, IDENTITY);
	matrices_set(&movipr.projection, 0);
	matrices_set(&movipr.mvp, IDENTITY);
	s = 1 / (tan(settings->fov * 0.5 * M_PI / 180.0));
	movipr.projection.m[0] = s / settings->aspect_ratio;
	movipr.projection.m[5] = s;
	movipr.projection.m[10] = -(settings->far + settings->near) /
		(settings->far - settings->near);
	movipr.projection.m[14] = -1;
	movipr.projection.m[11] = -1 * settings->far * settings->near /
		(settings->far - settings->near);
	return (movipr);
}

t_vec3			get_position_fixed_dist(t_vec3 target, float dist,
					float yaw, float pitch)
{
	t_vec3		position;
	float		yaw_rad;
	float		pitch_rad;

	yaw_rad = yaw * M_PI / 180.0f;
	pitch_rad = pitch * M_PI / 180.0f;
	position.v[0] = target.v[0] + cos(pitch_rad) * sin(yaw_rad) * dist;
	position.v[1] = target.v[1] + sin(pitch_rad) * dist;
	position.v[2] = target.v[2] + cos(pitch_rad) * cos(yaw_rad) * dist;
	return (position);
}

void			look_at(t_movipr *movipr, t_camera *cam)
{
	t_vec3		x_axis;
	t_vec3		y_axis;
	t_vec3		z_axis;
	t_mat4		orientation;
	t_mat4		translation;

	z_axis = normalize_vecteurs(substraction_vecteurs(cam->pos, cam->target));
	x_axis = normalize_vecteurs(cross_vecteurs(cam->up_vector, z_axis));
	y_axis = cross_vecteurs(z_axis, x_axis);
	matrices_set(&orientation, IDENTITY);
	matrices_set(&translation, IDENTITY);
	orientation.m[0] = x_axis.v[0];
	orientation.m[1] = x_axis.v[1];
	orientation.m[2] = x_axis.v[2];
	orientation.m[4] = y_axis.v[0];
	orientation.m[5] = y_axis.v[1];
	orientation.m[6] = y_axis.v[2];
	orientation.m[8] = z_axis.v[0];
	orientation.m[9] = z_axis.v[1];
	orientation.m[10] = z_axis.v[2];
	translation.m[3] = -cam->pos.v[0];
	translation.m[7] = -cam->pos.v[1];
	translation.m[11] = -cam->pos.v[2];
	movipr->view = matrices_multiplicate(orientation, translation);
}

void			compute_mvp(t_movipr *movipr)
{
	movipr->mvp = matrices_transpose(
		matrices_multiplicate(
			movipr->projection,
			matrices_multiplicate(
				movipr->view,
				movipr->model)));
}
