/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   events.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hbouillo <hbouillo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/24 01:57:51 by vtudes            #+#    #+#             */
/*   Updated: 2020/11/24 03:02:09 by hbouillo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

static void key_events_model(t_key *key, t_model *mod)
{
	if (key[KEY_RIGHT].code)
		mod->pos.v[0] += 0.1f;
	if (key[KEY_LEFT].code)
		mod->pos.v[0] -= 0.1f;
	if (key[KEY_UP].code)
		mod->pos.v[1] += 0.1f;
	if (key[KEY_DOWN].code)
		mod->pos.v[1] -= 0.1f;
	if (key[KEY_HOME].code)
		mod->pos.v[2] += 0.1f;
	if (key[KEY_END].code)
		mod->pos.v[2] -= 0.1f;
}

static void	key_events2(t_camera *cam, t_key *key, t_model *mod)
{
	if (key[KEY_J].code && cam->rota.distance <= ZOOM_MAX)
		cam->rota.distance += ZOOM_SPEED;
	if (key[KEY_K].code && cam->rota.distance >= ZOOM_MIN)
		cam->rota.distance -= ZOOM_SPEED;
	if (key[KEY_D].code)
		cam->rota.yaw -= 5.0f;
	if (key[KEY_A].code)
		cam->rota.yaw += 5.0f;
	if (key[KEY_W].code)
		cam->rota.pitch += 5.0f;
	if (key[KEY_S].code)
		cam->rota.pitch -= 5.0f;
	if (key[KEY_Q].code)
		cam->rota.roll += 5.0f;
	if (key[KEY_E].code)
		cam->rota.roll -= 5.0f;
	key_toggle(&key[KEY_T], &mod->target_opacity, 0, 1);
	key_toggle(&key[KEY_1], &mod->color, 0, 1);
	key_toggle(&key[KEY_2], &mod->greyscale, 0, 1);
	key_toggle(&key[KEY_3], &mod->wireframe, 0, 1);
	toggle_wireframe(mod);
	key_toggle(&key[KEY_C], &mod->shading, 0, 1);
}

static void	clamp_rot(t_camera *cam)
{
	if (cam->rota.pitch > 180.0f)
		cam->rota.pitch -= 360.0f;
	if (cam->rota.pitch < -180.0f)
		cam->rota.pitch += 360.0f;
	if (cam->rota.yaw > 360.0f)
		cam->rota.yaw -= 360.0f;
	if (cam->rota.yaw < 0.0f)
		cam->rota.yaw += 360.0f;
	if (cam->rota.roll > 360.0f)
		cam->rota.roll -= 360.0f;
	if (cam->rota.roll < 0.0f)
		cam->rota.roll += 360.0f;
}

void		key_events(GLFWwindow *window, t_camera *cam, t_model *mod)
{
	static t_key	key[MAX_KEYS];
	int				i;
	static char		space_pressed = 0;

	(void)mod;
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
	i = -1;
	while (++i < MAX_KEYS)
		key[i].code = glfwGetKey(window, i) == GLFW_PRESS ? 1 : 0;
	if (key[SPACE].code && space_pressed == 0)
	{
		space_pressed = 1;
		if (cam->auto_rot > 0.0f)
			cam->auto_rot = 0.0f;
		else
			cam->auto_rot = ROTATION_SPEED;
	}
	if (glfwGetKey(window, SPACE) == GLFW_RELEASE && space_pressed == 1)
		space_pressed = 0;
	clamp_rot(cam);
	key_events2(cam, key, mod);
	key_events_model(key, mod);
}

void		key_toggle(t_key *key, short *var, int v0, int v1)
{
	key->cooldown > 1 ? key->cooldown -= 1 : 0;
	if (key->code && key->cooldown <= 1)
	{
		if (*var == v1)
			*var = v0;
		else if (*var == v0)
			*var = v1;
		key->cooldown = COOLDOWN;
	}
}
