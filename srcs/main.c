/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vtudes <vtudes@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/24 01:57:37 by vtudes            #+#    #+#             */
/*   Updated: 2020/09/25 06:51:35 by vtudes           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

static int			init_glfw(void)
{
	glewExperimental = GL_TRUE;
	if (!glfwInit())
		return (-1);
	return (0);
}

static GLFWwindow	*init_window(void)
{
	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	return (glfwCreateWindow(WINDOW_W, WINDOW_H, "Scop tests", NULL, NULL));
}

static int			init_glew(void)
{
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
		return (-1);
	return (0);
}

int					main(int ac, char **av)
{
	t_obj			obj;
	t_settings		settings;
	t_camera		camera;
	GLFWwindow		*window;
	char			*filename;

	if (!(ac == 2 && ft_strcmp(&av[1][ft_strlen(av[1]) - 4], ".obj") == 0))
		error("no obj file given.");
	filename = av[1];
	if (init_glfw() == -1)
		error("could not initialize GLFW.");
	if ((window = init_window()) == NULL)
		error("couldn't initialize GLFW window.");
	glfwMakeContextCurrent(window);
	if (init_glew() == -1)
		error("could not initialize GLEW.");
	settings = init_settings();
	camera = init_camera();
	camera.rota = init_rot();
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	obj = parse_object(open(filename, O_RDONLY));
	parse_bmp(&obj, "./ressources/chaton.bmp");
	view_object(&obj, &camera, &settings, window);
	return (0);
}
