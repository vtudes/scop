/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   loop_handling.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vtudes <vtudes@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/24 01:58:15 by vtudes            #+#    #+#             */
/*   Updated: 2020/09/24 01:58:16 by vtudes           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

t_camera			*auto_rot(t_camera *cam)
{
	if (cam->rota.pitch >= 90.0f || cam->rota.pitch <= -90.0f)
		cam->up_vector.v[1] = -1.0f;
	else
		cam->up_vector.v[1] = 1.0f;
	cam->rota.yaw += cam->auto_rot;
	cam->pos = get_position_fixed_dist(cam->target, cam->rota.distance, \
	cam->rota.yaw, cam->rota.pitch);
	return (cam);
}

void				init_loop(t_obj *obj, t_buffer *buf)
{
	bind_buffers(buf);
	fill_buffers(obj, GL_DYNAMIC_DRAW);
	create_texture(buf, obj);
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.09f, 0.12f, 0.15f, 1.0f);
}

void				end_loop(GLFWwindow *window)
{
	glfwSwapBuffers(window);
	glfwPollEvents();
}

void				draw_model(t_buffer *buf, t_obj *obj)
{
	glBindVertexArray(buf->vao);
	glDrawElements(GL_TRIANGLES, obj->indices_num, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

void				handle_textures(t_model *model, t_buffer *buf)
{
	toggle_texture(model);
	glBindTexture(GL_TEXTURE_2D, buf->texture);
}
