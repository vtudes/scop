/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_object.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vtudes <vtudes@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/24 02:04:06 by vtudes            #+#    #+#             */
/*   Updated: 2020/09/29 14:47:54 by vtudes           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

static GLfloat	*append_vertices(GLfloat *array, char *line, int *length)
{
	int		i;
	int		j;
	char	**tab;
	GLfloat	*new;

	tab = ft_strsplit(&line[1], ' ');
	*length += 6;
	new = (GLfloat*)malloc(sizeof(GLfloat) * *length);
	i = -1;
	while (++i < *length - 6)
		new[i] = array[i];
	free(array);
	array = new;
	j = -1;
	while (tab[++j] != NULL)
	{
		array[*length - 6 + j] = (GLfloat)ft_atof(tab[j]);
		array[*length - 3 + j] = j * 0.66f;
		ft_strdel(&tab[j]);
	}
	ft_strdel(&tab[j]);
	free(tab);
	tab = NULL;
	return (array);
}

static GLuint	*append_indices(GLuint *array, char *line, int *length)
{
	int		j;
	int		m;
	char	**tab;

	tab = ft_strsplit(&line[1], ' ');
	m = array_len((void**)tab) == 4 ? 6 : 3;
	*length += m;
	array = gluint_array_copy(array, *length, m);
	j = -1;
	while (++j < 3)
	{
		array[*length - m + j] = (GLuint)ft_atoi(tab[j]) - 1;
		if (m == 6)
			array[*length - m + 3 + j] =
			(GLuint)ft_atoi(tab[j > 0 ? j + 1 : 0]) - 1;
		ft_strdel(&tab[j]);
	}
	ft_strdel(&tab[j]);
	free(tab);
	tab = NULL;
	return (array);
}

static t_vec3	compute_center_axis(GLfloat *vertices, int num_vertices)
{
	int		i;
	t_vec3	max;
	t_vec3	min;
	t_vec3	center;

	i = 0;
	max = vec3(0, 0, 0);
	min = vec3(0, 0, 0);
	while (i < num_vertices - 6)
	{
		vertices[i] > max.v[0] ? max.v[0] = vertices[i] : 0;
		vertices[i] < min.v[0] ? min.v[0] = vertices[i] : 0;
		vertices[i + 1] > max.v[1] ? max.v[1] = vertices[i + 1] : 0;
		vertices[i + 1] < min.v[1] ? min.v[1] = vertices[i + 1] : 0;
		vertices[i + 2] > max.v[2] ? max.v[2] = vertices[i + 2] : 0;
		vertices[i + 2] < min.v[2] ? min.v[2] = vertices[i + 2] : 0;
		i += 6;
	}
	center = fmulti_vecteurs(addition_vecteurs(max, min), 0.5);
	return (center);
}

static void		center_vertex(t_obj *obj, int length)
{
	int		i;
	float	tx;
	float	theta;

	i = 0;
	theta = 90 * (M_PI / 180);
	while (i < length)
	{
		obj->vertex[i] -= obj->axis_center.v[0];
		obj->vertex[i + 1] -= obj->axis_center.v[1];
		obj->vertex[i + 2] -= obj->axis_center.v[2];
		tx = obj->vertex[i] * cos(theta) -
			obj->vertex[i + 2] * sin(theta);
		obj->vertex[i + 2] = obj->vertex[i] * sin(theta) +
			obj->vertex[i + 2] * cos(theta);
		obj->vertex[i] = tx;
		i += 6;
	}
	obj->axis_center = vec3(0, 0, 0);
}

t_obj			parse_object(int fd)
{
	t_obj	obj;
	int		v;
	int		f;
	char	*line;

	v = 0;
	f = 0;
	malloc_obj(&obj);
	if (fd == -1)
		error("obj file opening failed.\n");
	while (get_next_line(fd, &line) > 0)
	{
		if (line[0] == 'v' && line[1] == ' ')
			obj.vertex = append_vertices(obj.vertex, line, &v);
		else if (line[0] == 'f' && line[1] == ' ')
			obj.indices = append_indices(obj.indices, line, &f);
		ft_strdel(&line);
	}
	ft_strdel(&line);
	obj.vertex_size = v * sizeof(GLfloat);
	obj.indices_size = f * sizeof(GLuint);
	obj.indices_num = f;
	obj.axis_center = compute_center_axis(obj.vertex, v);
	center_vertex(&obj, v);
	return (obj);
}
