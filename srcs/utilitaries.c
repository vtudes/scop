/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utilitaries.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vtudes <vtudes@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/25 07:02:52 by vtudes            #+#    #+#             */
/*   Updated: 2020/09/29 14:49:03 by vtudes           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

void	malloc_obj(t_obj *obj)
{
	obj->vertex = (GLfloat*)malloc(sizeof(GLfloat) * 3);
	obj->indices = (GLuint*)malloc(sizeof(GLuint) * 3);
}

int		array_len(void **tab)
{
	int	i;

	i = 0;
	while (tab[i] != NULL)
		i++;
	return (i);
}

void	error(char *info)
{
	ft_putstr("Error: ");
	ft_putstr(info);
	ft_putstr("\n");
	exit(0);
}

GLuint	*gluint_array_copy(GLuint *array, int length, int m)
{
	int		i;
	GLuint	*new;

	i = -1;
	new = (GLuint*)malloc(sizeof(GLuint) * length);
	while (++i < length - m)
		new[i] = array[i];
	free(array);
	array = new;
	return (new);
}

void	display_help(void)
{
	ft_putstr("  Usage:\n\t./scop [.obj filename]\n");
	ft_putstr("\t'W, A, S, D, shift, ctrl' Camera movement.\n");
	ft_putstr("\t'1' Color palette modifier.\n");
	ft_putstr("\t'2' Greyscale modifier.\n");
	ft_putstr("\t'3' Wireframe modifier.\n");
	ft_putstr("\t'Space' Lock / Unlock auto rotation.\n");
	ft_putstr("\t'J, M' Zoom In / Zoom out.\n");
	ft_putstr("\t'C' Shading Modifier.\n");
	ft_putstr("\t'T' Apply / Unapply textures.\n");
}
