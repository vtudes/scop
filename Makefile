NAME = scop
CC = gcc
UNAME := $(shell uname)

SRC_PATH = ./srcs/
OBJ_PATH = ./obj/
LIB_PATH = ./lib/
DEP_PATH = ./dep/
INC_PATH = ./includes/ $(LIB_PATH)libft/includes/ $(LIB_PATH)glfw/include/ $(LIB_PATH)glew/include \
			$(LIB_PATH)libmatrices/includes/

GCC_FLGS = -Werror -Wextra -Wall -pedantic -g3

GCC_LIBS = -lglfw3 -lGLEW
ifeq ($(UNAME), Linux)
GCC_LIBS += -lGL -lX11 -lpthread -ldl -lm
else
GCC_LIBS += -framework AppKit -framework OpenGL -framework IOKit -framework CoreVideo
endif
GCC_LIBS += -lft -lmatrices

SRC_NAME =	main.c\
			shaders.c\
			buffers.c\
			parse_object.c\
			render.c\
			mvp.c\
			utilitaries.c\
			events.c\
			loop_handling.c\
			parse_texture.c\
			toggle_wireframe.c\
			init_main.c\
			compute_and_draw.c\
			free_all.c\

OBJ_NAME = $(SRC_NAME:.c=.o)

SRC = $(addprefix $(SRC_PATH), $(SRC_NAME))
OBJ = $(addprefix $(OBJ_PATH), $(OBJ_NAME))
INC = $(addprefix -I,$(INC_PATH))
LIB = -L$(DEP_PATH)

all: $(NAME)

$(NAME): $(OBJ)
	mkdir -p $(DEP_PATH)
	make -C $(LIB_PATH)libft -j
	cp $(LIB_PATH)libft/libft.a $(DEP_PATH)
	make -C $(LIB_PATH)libmatrices -j
	cp $(LIB_PATH)libmatrices/libmatrices.a $(DEP_PATH)
	cd $(LIB_PATH)glfw && cmake .
	make -C $(LIB_PATH)glfw -j
	cp $(LIB_PATH)glfw/src/libglfw3.a $(DEP_PATH)
	make -C $(LIB_PATH)glew -j
	cp $(LIB_PATH)glew/lib/libGLEW.a $(DEP_PATH)
	$(CC) $(GCC_FLGS) $(INC) $(OBJ) $(LIB) $(GCC_LIBS) -o $(NAME)

$(OBJ_PATH)%.o: $(SRC_PATH)%.c
	mkdir -p $(OBJ_PATH)
	$(CC) $(GCC_FLGS) $(INC) -o $@ -c $<

clean:
	rm -fv $(OBJ)
	rm -rf $(OBJ_PATH)

fclean: clean
	make -C $(LIB_PATH)libft fclean
	make -C $(LIB_PATH)libmatrices fclean
	rm -f $(LIB_PATH)glfw/CMakeCache.txt
	make -C $(LIB_PATH)glfw clean
	make -C $(LIB_PATH)glew clean
	rm -fv $(NAME)
	rm -f $(DEP_PATH)libft.a
	rm -f $(DEP_PATH)libmatrices.a
	rm -f $(DEP_PATH)libglfw3.a
	rm -f $(DEP_PATH)libGLEW.a

re: fclean all
